/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import Dynamsoft from 'dwt';
import { WebTwain } from 'dwt/WebTwain';
import { DWTInitialConfig } from 'dwt/Dynamsoft';
import { Subject } from 'rxjs';
import { DeviceConfiguration, ScanSetup } from 'dwt/WebTwain.Acquire';
import { DynamsoftEnums } from 'dwt/Dynamsoft.Enum';
import { environment } from '../../../environments/environment'

@Injectable()
export class dwtService {
    protected _DWObject: WebTwain = null;
    protected _DWObjectEx: WebTwain = null;
    public runningEnvironment = Dynamsoft.Lib.env;
    public bUseService = false;
    public bWASM: boolean = false;
    bufferSubject: Subject<string> = new Subject<string>();
    public bUseCameraViaDirectShow: boolean = false;
    public _selectedDevice: string = "";
    protected _useCamera: boolean;
    protected _scannersCount: number;
    public devices: Device[] = [];
    barcodeSubject: Subject<any> = new Subject<any>();
    generalSubject: Subject<any> = new Subject<any>();
    private fileSavingPath = "C:";
    private fileActualName = "";
    constructor() {
        /**
         * ResourcesPath & ProductKey must be set in order to use the library!
         */
        Dynamsoft.WebTwainEnv.ResourcesPath = environment.properties.DynamsoftResourcesPath;
        Dynamsoft.WebTwainEnv.ProductKey = environment.properties.DynamsoftDwtProductKey;
        /**
         * ConnectToTheService is overwritten here for smoother install process.
         */
        Dynamsoft.WebTwainEnv.ConnectToTheService = () => {
            this.mountDWT();
        };
    }

    mountDWT(UseService?: boolean): Promise<any> {
        this._DWObject = null;
        return new Promise((res, rej) => {
            let dwtInitialConfig: DWTInitialConfig = {
                WebTwainId: "dwtObject"
            };
            /**
             * [Why checkScript()?]
             * Dynamic Web TWAIN relies on a few extra scripts to work correct.
             * Therefore we must make sure these files are ready before creating a WebTwain instance.
             */
            let checkScript = () => {
                if (Dynamsoft.Lib.detect.scriptLoaded) {
                    /*  Dynamsoft.WebTwainEnv.OnWebTwainPreExecute = () => {
                        // Show your own progress indicator
                        console.log('An operation starts!');
                      };
                      Dynamsoft.WebTwainEnv.OnWebTwainPostExecute = () => {
                        // Hide the progress indicator
                        console.log('An operation ends!');
                      };
                      */
                    if (this.runningEnvironment.bMobile) {
                        Dynamsoft.WebTwainEnv.UseLocalService = false;
                    } else {
                        if (UseService !== undefined)
                            Dynamsoft.WebTwainEnv.UseLocalService = UseService;
                        else {
                            Dynamsoft.WebTwainEnv.UseLocalService = this.bUseService;
                        }
                    }
                    this.bWASM = this.runningEnvironment.bMobile || !Dynamsoft.WebTwainEnv.UseLocalService;

                    Dynamsoft.WebTwainEnv.CreateDWTObjectEx(
                        dwtInitialConfig,
                        (_DWObject) => {
                            this._DWObject = _DWObject;
                            /*this._DWObject.IfShowProgressBar = false;
                            this._DWObject.IfShowCancelDialogWhenImageTransfer = false;*/
                            /**
                             * The event OnBitmapChanged is used here for monitoring the image buffer.
                             */
                            this._DWObject.RegisterEvent("OnBitmapChanged", (changedIndexArray, operationType, changedIndex, imagesCount) => {
                                switch (operationType) {
                                    /** reserved space
                                     * type: 1-Append(after index), 2-Insert(before index), 3-Remove, 4-Edit(Replace), 5-Index Change
                                     */
                                    case 1: break;
                                    case 2: break;
                                    case 3: break;
                                    case 4: break;
                                    case 5: break;
                                    default: break;
                                }
                                this.bufferSubject.next("changed");
                                if (this._DWObject.HowManyImagesInBuffer === 0)
                                    this.bufferSubject.next("empty");
                                else
                                    this.bufferSubject.next("filled");
                            });
                            res(_DWObject);
                        },
                        (errorString) => {
                            rej(errorString);
                        }
                    );
                } else {
                    setTimeout(() => checkScript(), 100);
                }
            };
            checkScript();
        });
    }

    acquire(config?: DeviceConfiguration | ScanSetup, bAdvanced?: boolean): Promise<any> {
        return new Promise((res, rej) => {
            if (this._selectedDevice !== "") {
                if (this._useCamera) {
                    if (this._DWObjectEx) {
                        if (this.bUseCameraViaDirectShow) {
                            this._DWObjectEx.Addon.Webcam.CaptureImage(() => {
                                this.getBlob([0], Dynamsoft.EnumDWT_ImageType.IT_PNG, this._DWObjectEx)
                                    .then(blob => this._DWObject.LoadImageFromBinary(blob, () => {
                                        this._DWObjectEx.RemoveImage(0);
                                        res(true);
                                    }, (errCode, errString) => rej(errString)));
                            }, (errCode, errStr) => rej(errStr));
                        }
                        else {
                            this._DWObjectEx.Addon.Camera.capture()
                                .then(blob => this._DWObject.LoadImageFromBinary(blob, () => {
                                    this._DWObjectEx.RemoveImage(0);
                                    res(true);
                                }, (errCode, errString) => rej(errString)));
                        }
                    } else {
                        rej("No WebTwain instanance for camera capture!");
                    }
                } else {
                    this._DWObject.SetOpenSourceTimeout(3000);
                    if (this._DWObject.OpenSource()) {
                        if (bAdvanced) {
                            this._DWObject.startScan(<ScanSetup>config);
                        } else {
                            this._DWObject.AcquireImage(<DeviceConfiguration>config, () => {
                                this._DWObject.CloseSource();
                                this._DWObject.CloseWorkingProcess();
                                res(true);
                            }, (errCode, errString) => {
                                rej(errString);
                            });
                        }
                    } else {
                        rej(this._DWObject.ErrorString);
                    }
                }
            } else {
                rej("Please select a device first!");
            }
        });
    }

    getBlob(indices: number[], type: DynamsoftEnums.EnumDWT_ImageType, dwt?: WebTwain): Promise<any> {
        return new Promise((res, rej) => {
            let _dwt = this._DWObject;
            if (dwt)
                _dwt = dwt;
            switch (type) {
                case Dynamsoft.EnumDWT_ImageType.IT_ALL:
                    rej("Must specify an image type!"); break;
            }
            _dwt.ConvertToBlob(indices, type, (result, indices, type) => {
                res(result);
            }, (errCode, errString) => {
                rej(errString);
            });
        });
    }

    mountVideoContainer(): Promise<any> {
        this._DWObjectEx = null;
        return new Promise((res, rej) => {
            if (this._DWObject) {
                let dwtInitialConfig: DWTInitialConfig = {
                    WebTwainId: "videoContainer"
                };
                Dynamsoft.WebTwainEnv.CreateDWTObjectEx(
                    dwtInitialConfig,
                    (_container) => {
                        this._DWObjectEx = _container;
                        res(_container);
                    },
                    (errorString) => {
                        rej(errorString);
                    }
                );
            } else {
                rej("Please call mountDWT first!");
            }
        });
    }
    /**
     * Removes the extra WebTwain instance. Optional.
     */
    unMountVideoContainer(): Promise<any> {
        return new Promise((res, rej) => {
            if (Dynamsoft.WebTwainEnv.DeleteDWTObject("videoContainer"))
                res(true);
            else
                rej(false);
        });
    }
    /**
     * Retrieve all devices (scanners + cameras).
     */
    getDevices(): Promise<Device[]> {
        return new Promise((res, rej) => {
            let _dwt = this._DWObject;
            if (this._DWObjectEx)
                _dwt = this._DWObjectEx;
            this.devices = [];
            let count = this._DWObject.SourceCount;
            let _scanners = <string[]>(this._DWObject.GetSourceNames());
            if (count !== _scanners.length) {
                rej('Possible wrong source count!');//not likely to happen
            }
            for (let i = 0; i < _scanners.length; i++) {
                this.devices.push({ deviceId: Math.floor(Math.random() * 100000).toString(), name: (i + 1).toString() + "." + _scanners[i], label: _scanners[i], type: "scanner" });
            }
            this._scannersCount = this.devices.length;
            if (this.bUseCameraViaDirectShow) {
                try {
                    let _cameras = _dwt.Addon.Webcam.GetSourceList();
                    for (let i = 0; i < _cameras.length; i++) {
                        this.devices.push({ deviceId: Math.floor(Math.random() * 100000).toString(), name: (i + 1).toString() + "." + _cameras[i], label: _cameras[i], type: "camera" });
                    }
                    res(this.devices);
                } catch (e) {
                    rej(e);
                }
            } else {
                _dwt.Addon.Camera.getSourceList()
                    .then(_cameras => {
                        for (let i = 0; i < _cameras.length; i++) {
                            this.devices.push({ deviceId: _cameras[i].deviceId, name: (i + 1).toString() + "." + _cameras[i].label, label: _cameras[i].label, type: "camera" });
                        }
                        res(this.devices);
                    }, err => rej(err));
            }
        });
    }
    /**
     * Retrieve detailed information of the devices.
     */
    getDeviceDetails() {
        return this._DWObject.GetSourceNames(true);
    }

    /**
 * Load images by opending a file dialog (either with built-in feature or use a INPUT element).
 * @param files Files to load.
 */
    load(files?: FileList): Promise<any> {
        return new Promise((res, rej) => {
            this._DWObject.Addon.PDF.SetConvertMode(Dynamsoft.EnumDWT_ConvertMode.CM_DEFAULT);
            this._DWObject.Addon.PDF.SetResolution(200);
            if (this.bWASM && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    this._DWObject.LoadImageFromBinary(files[i], () => { res(true); }, (errCode, errString) => { rej(errString); })
                }
            } else {
                let filter = "BMP,TIF,JPG,PNG,PDF|*.bmp;*.tif;*.png;*.jpg;*.pdf;*.tiff;*.jpeg";
                if (Dynamsoft.Lib.env.bMac)
                    filter = "TIF,TIFF,JPG,JPEG,PNG,PDF";
                this._DWObject.IfShowFileDialog = true;
                this._DWObject.RegisterEvent("OnPostLoad", (
                    directory: string,
                    fileName: string,
                    fileType: DynamsoftEnums.EnumDWT_ImageType) => {
                });
                this._DWObject.RegisterEvent("OnGetFilePath", (isSave, filesCount, index, directory, fileName) => {
                    if (index === filesCount - 1)
                        this._DWObject.LoadImage(directory + "\\" + fileName, () => { res(true); }, (errCode, errStr) => rej(errStr));
                    else
                        this._DWObject.LoadImage(directory + "\\" + fileName, () => { }, (errCode, errStr) => rej(errStr));
                });
                this._DWObject.ShowFileDialog(false, filter, 0, "", "", true, false, 0);
            }
        });
    }

    uploadToServer(indices: number[], type: DynamsoftEnums.EnumDWT_ImageType, fileName: string): Promise<any> {
        return new Promise((res, rej) => {
            fileName = fileName + this.getExtension(type);
            let url = "", savedDir = "";
            if (environment.properties.uploadTargetURL !== "") {
                url = environment.properties.uploadTargetURL;
                /**
                 * Change this to point to a url of a directory under which the uploaded files are saved.
                 */
                savedDir = "";
            }
            else {
                /**
                 * Testing server based on express & formidable
                 * Make sure you have the server in /server/ running
                 */
                let protocol = Dynamsoft.Lib.detect.ssl ? "https://" : "http://"
                let _strPort = 2020;
                let strActionPage = "/upload";
                savedDir = protocol + window.location.hostname + ":" + _strPort + "/uploaded/";
                url = protocol + window.location.hostname + ":" + _strPort + strActionPage;
            }
            this._DWObject.HTTPUpload(
                url,
                indices,
                type,
                Dynamsoft.EnumDWT_UploadDataFormat.Binary,
                fileName,
                () => {
                    res({ name: fileName, url: savedDir + fileName });
                },
                (errCode, errString, responseStr) => {
                    if (responseStr !== "") {
                        this.generalSubject.next({ type: "httpResponse", responsString: responseStr });
                    }
                    rej(errString);
                }
            );
        });
    }

    getExtension(type: DynamsoftEnums.EnumDWT_ImageType) {
        switch (type) {
            case 0: return ".bmp";
            case 1: return ".jpg";
            case 2: case 8: return ".tif";
            case 3: return ".png";
            case 4: case 7: return ".pdf";
            default: return ".unknown";
        }
    }

    saveLocally(indices: number[], type: DynamsoftEnums.EnumDWT_ImageType, fileName: string, showDialog: boolean): Promise<any> {
        return new Promise((res, rej) => {
            let saveInner = (_path, _name, _type): Promise<any> => {
                return new Promise((res, rej) => {
                    let s = () => {
                        if (showDialog) {
                            _name = this.fileActualName;
                            _path = this.fileSavingPath + "\\" + _name;
                        }
                        res({ name: _name, path: _path });
                    }, f = (errCode, errStr) => rej(errStr);
                    switch (_type) {
                        case 0: this._DWObject.SaveAsBMP(_path, indices[0], s, f); break;
                        case 1: this._DWObject.SaveAsJPEG(_path, indices[0], s, f); break;
                        case 2: this._DWObject.SaveAsTIFF(_path, indices[0], s, f); break;
                        case 3: this._DWObject.SaveAsPNG(_path, indices[0], s, f); break;
                        case 4: this._DWObject.SaveAsPDF(_path, indices[0], s, f); break;
                        case 7: this._DWObject.SaveSelectedImagesAsMultiPagePDF(_path, s, f); break;
                        case 8: this._DWObject.SaveSelectedImagesAsMultiPageTIFF(_path, s, f); break;
                        default: break;
                    }
                });
            };
            fileName = fileName + this.getExtension(type);
            let filePath = this.fileSavingPath + "\\" + fileName;
            if (showDialog) {
                this.fileSavingPath = "";
                this.fileActualName = "";
                this._DWObject.IfShowFileDialog = true;
                this._DWObject.RegisterEvent("OnGetFilePath", (isSave, filesCount, index, directory, _fn) => {
                    if (directory === "" && _fn === "") {
                        rej("User cancelled the operation.")
                    } else {
                        this.fileActualName = _fn;
                        this.fileSavingPath = directory;
                    }
                });
                res(saveInner(this.fileSavingPath + "\\" + fileName, fileName, type));
            } else {
                this._DWObject.IfShowFileDialog = false;
                res(saveInner(filePath, fileName, type));
            }
        });
    }

    getBase64(indices: number[], type: DynamsoftEnums.EnumDWT_ImageType, dwt?: WebTwain): Promise<any> {
    return new Promise((res, rej) => {
      let _dwt = this._DWObject;
      if (dwt)
        _dwt = dwt;
      if (type === Dynamsoft.EnumDWT_ImageType.IT_ALL)
        rej("Must specify an image type!");
      _dwt.ConvertToBase64(indices, type, (result, indices, type) => {
        let _result = result.getData(0, result.getLength());
        switch (type) {
          case 0:
            res("data:image/bmp;base64," + _result); break;
          case 1:
            res("data:image/jpeg;base64," + _result); break;
          case 2:
          case 8:
            res("data:image/tiff;base64," + _result); break;
          case 3:
            res("data:image/png;base64," + _result); break;
          case 4:
          case 7:
            res("data:application/pdf;base64," + _result); break;
          default:
            rej("Wrong image type!"); break;
        }
      }, (errCode, errString) => {
        rej(errString);
      });
    });
  }
}

export interface Device {
    deviceId: string,
    name: string,
    label: string,
    type: string
}