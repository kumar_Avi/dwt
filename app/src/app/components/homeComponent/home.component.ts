/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { dwtService, Device } from '../../services/dwt/dwt.service';
import { WebTwain } from 'dwt/WebTwain';
import { BasicViewerConfig } from 'dwt/WebTwain.Viewer';
import { NgbModal, NgbModalRef, } from '@ng-bootstrap/ng-bootstrap';
import Dynamsoft from 'dwt';
import { Subscription, Observable, empty } from 'rxjs';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example : 
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-home',
    templateUrl: './home.template.html'
})

export class homeComponent extends NBaseComponent implements OnInit {
    public dwtMounted = false;
    public containerId = "dwtcontrolContainer";
    public bWASM = false;
    protected DWObject: WebTwain = null;
    public bUseCameraViaDirectShow: boolean;
    protected VideoContainer: WebTwain = null;
    public basicView: BasicViewerConfig;
    public zones: Zone[] = [];
    public bMobile: boolean;
    public instantError: string = "There is no image in buffer!";
    private modalRef: NgbModalRef;
    public editorShown = false;
    public deviceName: string = "Choose...";
    public videoPlaying: boolean = false;
    public showVideoText: string = "Show Video";
    public videoContainerId = "videoContainer";
    private bufferObservable: Observable<string>;

    public formatsToImport = {
        JPG: true,
        PNG: true,
        TIF: false,
        PDF: false
    };
    public scanOptions = {
        IfShowUI: true,
        PixelType: "1",// "gray"
        Resolution: 200,
        IfFeederEnabled: false,
        IfDuplexEnabled: false,
        IfDisableSourceAfterAcquire: true,
        IfGetImageInfo: false,
        IfGetExtImageInfo: false,
        extendedImageInfoQueryLevel: 0
    };
    public saveOptions = {
        outPutType: "File",
        outPutFormat: "PDF",
        multiPage: false,
        fileName: "CreatedByDynamsoft",
        upload: true,
        buttonText: "Upload",
        indices: []
    };
    public saveResults = {
        blob: [],
        blobURL: [],
        base64String: [],
        savedFiles: [],
        uploadedFiles: [],
        base64ButtonText: [],
        saveFileText: [],
        blobToShow: null
    }
    public cameraOptions = [];
    public currentItem = "";
    public currentOption = "";
    public currentOptionItems = [];
    public emptyBuffer: boolean = true;
    public devices: Device[];
    public showDevices: boolean = false;
    private bufferSubscription: Subscription;

    constructor(protected dwtService: dwtService, private modalService: NgbModal) {
        super();
        this.initDWT();
    }

    ngOnInit() {
        this.bufferObservable = this.dwtService.bufferSubject;
        this.bufferSubscription = this.bufferObservable.subscribe(
            bufferStatus => {
                this.emptyBuffer = false;
                switch (bufferStatus) {
                    default: break;
                    case "empty": this.emptyBuffer = true; break;
                    case "changed":
                        this.zones = [];
                        break;
                }
                if (this.emptyBuffer) {
                    this.showMessage("There is no image in buffer!");
                } else { this.clearMessage() }
            }
        );

    }

    initDWT() {
        this.DWObject = null;
        this.dwtService.mountDWT()
            .then(
                obj => {
                    this.DWObject = obj;
                    this.bWASM = this.dwtService.bWASM;
                    this.bUseCameraViaDirectShow = this.dwtService.bUseCameraViaDirectShow;
                    this.dwtMounted = true;
                    this.dwtService.mountVideoContainer()
                        .then(containerDWT => {
                            this.VideoContainer = containerDWT;
                        }, err => this.showMessage(err));
                    setTimeout(() => {
                        this.bindViewer();
                        this.DWObject.Viewer['ImageMargin'] = 10;
                    }, 0);
                },
                err => this.showMessage(err));
    }

    bindViewer() {
        this.basicView = {
            Height: "100%",
            Width: "100%",
            view: { bShow: true, Width: "80%" }
        }
        if (this.DWObject.BindViewer(this.containerId, this.basicView)) {
            // Remove the context menu which is still not functioning correctly.
            this.DWObject.Viewer.off('imageRightClick');
            this.DWObject.RegisterEvent('OnImageAreaSelected', (nImageIndex, left, top, right, bottom, sAreaIndex) => {
                this.clearMessage();
                if (sAreaIndex > this.zones.length + 1) {
                    this.showMessage("Impossible Area selected!");
                    return;
                }
                if (this.zones.length + 1 === sAreaIndex)
                    this.zones.push({ x: left, y: top, width: right - left, height: bottom - top, index: nImageIndex });
                else
                    this.zones.splice(sAreaIndex - 1, 1, { x: left, y: top, width: right - left, height: bottom - top, index: nImageIndex });
            });
            this.DWObject.RegisterEvent('OnImageAreaDeSelected', () => {
                this.clearMessage(); this.zones = [];
            });
            this.bMobile ? this.DWObject.Viewer.operationMode = 0 : this.DWObject.Viewer.operationMode = 1;
            this.DWObject.Viewer.showFooter = false;
            this.DWObject.Viewer.showHeader = false;
            this.DWObject.ShowPageNumber = true;
            //this.DWObject.Viewer.off('imageRightClick');
            this.bMobile ? this.DWObject.Viewer.setViewMode(1, 5) :
                this.DWObject.Viewer.setViewMode(1, 3);
            if (document.getElementById(this.containerId + "-fileInput"))
                // Only allow one such input on the page
                return;
            let WASMInput = document.createElement("input");
            WASMInput.style.position = "fixed";
            WASMInput.style.top = "-1000px";
            WASMInput.setAttribute("multiple", "multiple");
            WASMInput.setAttribute("id", this.containerId + "-fileInput");
            WASMInput.setAttribute("type", "file");
            WASMInput.onclick = _ => {
                let filters = [], filter = "";
                this.formatsToImport.JPG ? filters.push("image/jpeg") : false;
                this.formatsToImport.PNG ? filters.push("image/png") : false;
                this.formatsToImport.TIF ? filters.push("image/tiff") : false;
                this.formatsToImport.PDF ? filters.push("application/pdf") : false;
                if (filters.length > 0) {
                    filter = filters.join(",");
                    this.clearMessage();
                } else {
                    this.showMessage("Please select at least one format!");
                    return false;
                }
                WASMInput.setAttribute("accept", filter);
            }
            WASMInput.onchange = evt => {
                let _input = <HTMLInputElement>evt.target;
                this.dwtService.load(_input.files)
                    .then(_ => {
                        this.closeModal(true);
                        _input.value = '';
                    });
            };
            document.getElementById(this.containerId).parentElement.appendChild(WASMInput);
        }
        else {
            console.log(this.DWObject.ErrorString);
        }
    }

    acquire() {
        if (this.dwtService.bWASM) {
            (<HTMLInputElement>document.getElementById(this.containerId + "-fileInput")).value = "";
            document.getElementById(this.containerId + "-fileInput").click();
        } else {
            this.scan();
        }
    }

    save() {
        this.saveResults.uploadedFiles = [];
        this.saveResults.savedFiles = [];
        this.saveResults.base64String = [];
        this.saveResults.blob = [];
        this.saveResults.blobURL = [];
        this.saveResults.base64ButtonText = [];
        this.saveResults.saveFileText = [];
        switch (this.saveOptions.outPutType) {
            case "File":
                if (this.saveOptions.upload) {
                    if (this.saveOptions.multiPage) {
                        let selectedIndices = this.DWObject.SelectedImagesIndices;
                        this.dwtService.uploadToServer(selectedIndices, this.getImageType(this.saveOptions.outPutFormat), this.saveOptions.fileName)
                            .then(result => { this.saveResults.uploadedFiles.push(result); this.clearMessage(); }, err => this.showMessage(err));
                    }
                    else {
                        let count = this.DWObject.HowManyImagesInBuffer;
                        for (let i = 0; i < count; i++) {
                            this.dwtService.uploadToServer([i], this.getImageType(this.saveOptions.outPutFormat), this.saveOptions.fileName + "_" + (i + 1))
                                .then(result => { this.saveResults.uploadedFiles.push(result); this.clearMessage(); }, err => this.showMessage(err));
                        }
                    }
                } else {
                    if (this.saveOptions.multiPage) {
                        let selectedIndices = this.DWObject.SelectedImagesIndices;
                        let type = this.getImageType(this.saveOptions.outPutFormat);
                        if (type === 2) type = 8;
                        if (type === 4) type = 7;
                        this.dwtService.saveLocally(selectedIndices, type, this.saveOptions.fileName, true)
                            .then(result => {
                                this.saveResults.savedFiles.push(result);
                                this.saveResults.saveFileText.push("Copy path for " + result.name);
                                this.clearMessage();
                            }, err => this.showMessage(err));
                    }
                    else {
                        let count = this.DWObject.HowManyImagesInBuffer;
                        let fileName = this.saveOptions.fileName + "_" + 1;
                        this.dwtService.saveLocally([0], this.getImageType(this.saveOptions.outPutFormat), fileName, true)
                            .then(result => {
                                this.clearMessage();
                                this.saveResults.savedFiles.push(result);
                                this.saveResults.saveFileText.push("Copy path for  " + result.name);
                                /**
                                 * Save more only when the 1st one succeeds!
                                 */
                                for (let i = 1; i < count; i++) {
                                    let fileName = this.saveOptions.fileName + "_" + (i + 1);
                                    this.dwtService.saveLocally([i], this.getImageType(this.saveOptions.outPutFormat), fileName, false)
                                        .then(result => {
                                            this.clearMessage();
                                            this.saveResults.savedFiles.push(result);
                                            this.saveResults.saveFileText.push("Copy path for  " + result.name);
                                        }, err => this.showMessage(err));
                                }
                            }, err => this.showMessage(err));
                    }
                }
                break;
            case "Blob":
                if (this.saveOptions.multiPage) {
                    let selectedIndices = this.DWObject.SelectedImagesIndices;
                    this.dwtService.getBlob(
                        selectedIndices,
                        this.getImageType(this.saveOptions.outPutFormat))
                        .then(blob => {
                            let newFile = new File([blob], "Saved_Blob (" + blob.type + ")", { type: blob.type });
                            this.saveResults.blob.push(newFile);
                            this.saveResults.blobURL.push(URL.createObjectURL(newFile));
                            this.clearMessage();
                        }, err => this.showMessage(err));
                } else {
                    let count = this.DWObject.HowManyImagesInBuffer;
                    for (let i = 0; i < count; i++) {
                        this.dwtService.getBlob(
                            [i],
                            this.getImageType(this.saveOptions.outPutFormat))
                            .then(blob => {
                                let newFile = new File([blob], "Saved_Blob_" + (i + 1) + "(" + blob.type + ")", { type: blob.type });
                                this.saveResults.blob.push(newFile);
                                this.saveResults.blobURL.push(URL.createObjectURL(newFile));
                                this.clearMessage();
                            }, err => this.showMessage(err));
                    }
                }
                break;
            case "Base64":
                if (this.saveOptions.multiPage) {
                    let selectedIndices = this.DWObject.SelectedImagesIndices;
                    this.dwtService.getBase64(
                        selectedIndices,
                        this.getImageType(this.saveOptions.outPutFormat))
                        .then(base64String => {
                            this.saveResults.base64String.push(base64String);
                            this.saveResults.base64ButtonText.push("Copy Base64 String");
                            this.clearMessage();
                        }, err => this.showMessage(err));
                } else {
                    let count = this.DWObject.HowManyImagesInBuffer;
                    for (let i = 0; i < count; i++) {
                        this.dwtService.getBase64(
                            [i],
                            this.getImageType(this.saveOptions.outPutFormat))
                            .then(base64String => {
                                this.saveResults.base64String.push(base64String);
                                this.saveResults.base64ButtonText.push("Copy Base64 String for image " + i);
                                this.clearMessage();
                            }, err => this.showMessage(err));
                    }
                }
                break;
            default: break;
        }
    }

    scan() {
        this.dwtService.acquire(this.scanOptions)
            .then(() => {
                this.closeModal(true);
                if (!this.emptyBuffer)
                    this.clearMessage();
            }, err => this.showMessage(err));
    }

    openCamera() {
        this.DWObject.Viewer.showVideo();
    }
    playVideo() {
        let _dwt = this.DWObject;
        if (this.bUseCameraViaDirectShow) {
            this.DWObject.Addon.Webcam.StopVideo();
            this.DWObject.Addon.Webcam.CloseSource();
        }
        else
            this.DWObject.Addon.Camera.stop();
        if (this.VideoContainer)
            _dwt = this.VideoContainer;
        if (this.VideoContainer === null) {
            this.showMessage("No Video Container!");
            return false;
        }
        if (this.bUseCameraViaDirectShow) {
            _dwt.Addon.Webcam.PlayVideo(_dwt, 80, () => {
                this.showVideoText = "Stop Video";
                this.videoPlaying = true;
            });
            return true;
        } else {
            _dwt.Addon.Camera.play(document.getElementById(this.videoContainerId))
                .then(() => {
                    this.showVideoText = "Stop Video";
                    this.videoPlaying = true;
                    return true;
                }, () => { return false; })
        }
    }
    showEditor() {
        this.DWObject.ShowImageEditor();
        this.DWObject.RegisterEvent('CloseImageEditorUI', () => {
            this.editorShown = false;
        });
        this.editorShown = true;
    }

    toggleVideo() {
        let _dwt = this.DWObject;
        if (this.VideoContainer)
            _dwt = this.VideoContainer;
        if (this.videoPlaying) {
            this.videoPlaying = false;
            this.showVideoText = "Show Video";
            if (this.bUseCameraViaDirectShow)
                return _dwt.Addon.Webcam.StopVideo();
            else
                return _dwt.Addon.Camera.stop();
        } else
            return this.playVideo();
    }

    openModal(content, type?: string) {
        this.modalRef = this.modalService.open(content, {
            backdrop: "static",
            centered: true,
            beforeDismiss: (): boolean => {
                switch (type) {
                    case "acquire":
                        this.deviceName = "Choose...";
                        return true;
                    case "camera":
                        if (this.bWASM)
                            return true;
                        if (this.videoPlaying) {
                            if (this.bUseCameraViaDirectShow) {
                                this.showMessage("Please stop video first!");
                                return false;
                            }
                            else {
                                this.toggleVideo();
                                return true;
                            }
                        } else {
                            this.deviceName = "Choose...";
                            return this.VideoContainer.UnbindViewer();
                        }
                    case "save":
                        this.saveOptions.indices = [];
                        return true;
                        break;
                    default:
                        return true;
                        break;
                }
            }
        });
        this.modalRef.result.then((result) => {//close
        }, (reason) => {//dismiss, reset modal dialogs
            if (this.emptyBuffer)
                this.showMessage("There is no image in buffer!");
            this.saveResults = {
                blob: [],
                blobURL: [],
                base64String: [],
                savedFiles: [],
                uploadedFiles: [],
                base64ButtonText: [],
                saveFileText: [],
                blobToShow: null
            };
        });
        switch (type) {
            case "camera":
                let makeSureDIVExists = () => {
                    let container = <HTMLDivElement>document.getElementById(this.videoContainerId);
                    if (container) {
                        if (this.VideoContainer === null) {
                            this.showMessage("No Video Container!");
                            return;
                        }
                        container.style.height = "100%";
                        this.cameraOptions = [];
                        this.currentOption = "";
                        this.currentItem = "";
                        this.currentOptionItems = [];
                        this.VideoContainer.BindViewer(this.videoContainerId, {
                            Height: "100%",
                            Width: "100%",
                            view: { bShow: false, Width: "80%" }
                        });
                    }
                    else
                        setTimeout(() => makeSureDIVExists(), 10);
                };
                makeSureDIVExists();
            case "acquire":
                this.clearMessage();
                this.dwtService.getDevices()
                    .then(result => { this.devices = result; this.showDevices = true; }, err => this.showMessage(err));
                this.deviceName = "Choose...";
                break;
            case "save":
                if (!this.emptyBuffer)
                    this.clearMessage();
                let selectedIndices = this.DWObject.SelectedImagesIndices;
                let count = this.DWObject.HowManyImagesInBuffer;
                for (let i = 0; i < count; i++)
                    this.saveOptions.indices.push({ number: i, selected: !!selectedIndices.find(o => { return o == i; }) });
                break;
            default: break;
        }
    }
    getImageType(formatString: string): number {
        formatString = "IT_" + formatString;
        let imageType = 4;
        Object.entries(Dynamsoft.EnumDWT_ImageType).forEach(_type => {
            if (_type[0] === formatString)
                imageType = <number>_type[1];
        });
        return imageType;
    }

    outPutTypeChanged(newType) {
        switch (newType) {
            case "File":
                this.saveOptions.upload ? this.saveOptions.buttonText = "Upload" : this.saveOptions.buttonText = "Save";
                break;
            case "Blob":
                this.saveOptions.buttonText = "Save As Blob";
                break;
            case "Base64":
                this.saveOptions.buttonText = "Save As Base64";
                break;
            default: break;
        }
    }

    handleOutPutFormatChange(format) {
        if (format !== "PDF" && format !== "TIF") {
            this.saveOptions.multiPage = false;
            this.handleMultiPageCheck();
        }
    }

    handleMultiPageCheck() {
        if (this.saveOptions.multiPage) {
            if (this.DWObject.SelectedImagesIndices.length === 1) {
                this.saveOptions.indices.forEach((value, index, arr) => { value.selected = true; arr[index] = value; });
                this.DWObject.SelectAllImages();
            }
        } else {
            if (this.DWObject.SelectedImagesIndices.length > 1)
                this.DWObject.SelectImages([this.DWObject.CurrentImageIndexInBuffer]);
            this.saveOptions.indices.forEach((value, index, arr) => {
                value.selected = false;
                if (value.number === this.DWObject.CurrentImageIndexInBuffer)
                    value.selected = true;
                arr[index] = value;
            });
        }
    }

    handleIndexSelection(selected: boolean, index: number) {
        let selectedIndices = this.DWObject.SelectedImagesIndices;
        if (selected) {
            selectedIndices.push(index);
        } else {
            selectedIndices.splice(selectedIndices.indexOf(index), 1);
        }
        selectedIndices.sort();
        this.DWObject.SelectImages(selectedIndices);
    }

    copyURLToShow(url) {
        if (navigator.clipboard) {
            navigator.clipboard.writeText(url)
                .then(_ => {
                    this.showMessage("URL of the blob copied, try paste it in another tab to view it or download the blob as a file. Don't forget to add an extension to the downloaded file before opening it!");
                });
        } else {
            this.showMessage("Can not use the clipboard! Please use HTTPS.");
        }
    }

    copyFilePath(indexOfString) {
        if (navigator.clipboard) {
            if (this.saveResults.saveFileText[indexOfString] === "")
                this.showMessage("No resulting String!");
            navigator.clipboard.writeText(this.saveResults.savedFiles[indexOfString].path)
                .then(_ => {
                    this.saveResults.saveFileText[indexOfString] = "Copied!";
                    setTimeout(() => {
                        this.saveResults.savedFiles.splice(indexOfString, 1);
                        this.saveResults.saveFileText.splice(indexOfString, 1);
                    }, 3000);
                });
        } else {
            this.showMessage("Can not use the clipboard! Please use HTTPS.");
        }
    }

    copyBase64String(indexOfString) {
        if (navigator.clipboard) {
            if (this.saveResults.base64String[indexOfString] === "")
                this.showMessage("No resulting String!");
            navigator.clipboard.writeText(this.saveResults.base64String[indexOfString])
                .then(_ => {
                    this.saveResults.base64ButtonText[indexOfString] = "Copied!";
                    setTimeout(() => {
                        this.saveResults.base64String.splice(indexOfString, 1);
                        this.saveResults.base64ButtonText.splice(indexOfString, 1);
                    }, 3000);
                });
        } else {
            this.showMessage("Can not use the clipboard! Please use HTTPS.");
        }
    }

    clearMessage() {
        this.instantError = "";
    }
    showMessage(msg: string) {
        this.instantError = msg;
    }

    closeModal(close: boolean) {
        if (this.modalRef === undefined) return;
        if (close)
            this.modalRef.close();
        else
            this.modalRef.dismiss();
    }


}

interface Zone {
    x: number;
    y: number;
    width: number;
    height: number;
    index: number;
}
