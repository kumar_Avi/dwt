export const environment = {
    "name": "prod",
    "properties": {
        "production": true,
        "ssdURL": "http://localhost:8081/api/",
        "tenantName": "neutrinos",
        "appName": "dwt-angular",
        "namespace": "com.neutrinos.dwt-angular",
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "useDefaultExceptionUI": true,
        "isIDSEnabled": "false",
        "webAppMountpoint": "web",
        "NGFORAGE_MOBILE_DRIVER": "INDEXED_DB",
        "DynamsoftResourcesPath": "assets/dwt-resources",
        "DynamsoftDwtProductKey": "t01016QAAAH4YXRLYSK0NwmVz0WcbLHUIJGZ3fhk8BeiP6p2/dkEu8o1tjz2Y3GBwnUGV2NCSkRj6YD4XMWCyZaaV1HQr/nSsDTkyD1zoa3PEGn1/tDAtTJ0GVZiiDEOFxSTrDdvOL70=",
        "uploadTargetURL": ""
    }
}